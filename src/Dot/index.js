import React, {PureComponent} from 'react';
import classNames from 'classnames';
import styles from './Dot.scss';

export default class Dot extends PureComponent {
  
  render() {
    const {
      dayItem,
      enableScheduled
    } = this.props;
  
    let dot = null;
    if (dayItem) {
      dot = <span className={classNames(styles.root, {
          [styles.published]: dayItem.data.published === 1,
          [styles.unpublished]: !enableScheduled && dayItem.data.published != 1,
          [styles.scheduled]: enableScheduled && dayItem.data.published != 1,
        })}>
      </span>
    }

    return dot;
  }
}
