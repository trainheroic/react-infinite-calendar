function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { PureComponent } from 'react';
import classNames from 'classnames';
var styles = {
  'root': 'Cal__Dot__root',
  'unpublished': 'Cal__Dot__unpublished',
  'published': 'Cal__Dot__published',
  'scheduled': 'Cal__Dot__scheduled'
};

var Dot = function (_PureComponent) {
  _inherits(Dot, _PureComponent);

  function Dot() {
    _classCallCheck(this, Dot);

    return _possibleConstructorReturn(this, _PureComponent.apply(this, arguments));
  }

  Dot.prototype.render = function render() {
    var _props = this.props,
        dayItem = _props.dayItem,
        enableScheduled = _props.enableScheduled;


    var dot = null;
    if (dayItem) {
      var _classNames;

      dot = React.createElement('span', { className: classNames(styles.root, (_classNames = {}, _classNames[styles.published] = dayItem.data.published === 1, _classNames[styles.unpublished] = !enableScheduled && dayItem.data.published != 1, _classNames[styles.scheduled] = enableScheduled && dayItem.data.published != 1, _classNames)) });
    }

    return dot;
  };

  return Dot;
}(PureComponent);

export { Dot as default };