import React from 'react';
import {render} from 'react-dom';
import InfiniteCalendar from '../../src';
import '../../styles.css';
import './demo.css';

let calOptions = {
    showHeader: false
};

render(
  <InfiniteCalendar
    width={265}
    height={265}
    rowHeight={40}
    enableScheduled={true}
    displayOptions={calOptions}
  />
, document.querySelector('#demo'));
